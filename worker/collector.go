/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package worker

import (
	"encoding/json"
	"os/exec"

	log "github.com/Sirupsen/logrus"
	conf "gitlab.cern.ch/fts/fts-config-collector/config"
	trans "gitlab.cern.ch/fts/fts-config-collector/transformer"
)

func sendShares(data []map[string]interface{}, fts_server string, forwarder *Forwarder) {
	var err error
	var shares []map[string]interface{}
	var message []byte
	if shares = trans.TransformShares(data, fts_server); len(shares) != 0 {
		for i := 0; i < len(shares); i++ {
			if message, err = json.Marshal(shares[i]); err != nil {
				log.WithError(err).Error("Creating message in " + string(conf.Shares))
			}
			if err = forwarder.ForwardSend(string(message), conf.Shares); err != nil {
				log.WithError(err).Error("Sending " + string(conf.Shares))
			}
		}
	}
}

func sendLinks(data []map[string]interface{}, fts_server string, forwarder *Forwarder) {
	var err error
	var links []map[string]interface{}
	var message []byte
	if links = trans.TransformLinks(data, fts_server); len(links) != 0 {
		for i := 0; i < len(links); i++ {
			if message, err = json.Marshal(links[i]); err != nil {
				log.WithError(err).Error("Creating message in " + string(conf.Links))
			}
			if err = forwarder.ForwardSend(string(message), conf.Links); err != nil {
				log.WithError(err).Error("Sending " + string(conf.Links))
			}
		}
	}
}

func sendFixed(data []map[string]interface{}, fts_server string, forwarder *Forwarder) {
	var err error
	var fixed []map[string]interface{}
	var message []byte
	if fixed = trans.TransformFixed(data, fts_server); len(fixed) != 0 {
		for i := 0; i < len(fixed); i++ {
			if message, err = json.Marshal(fixed[i]); err != nil {
				log.WithError(err).Error("Creating message in " + string(conf.Fixed))
			}
			if err = forwarder.ForwardSend(string(message), conf.Fixed); err != nil {
				log.WithError(err).Error("Sending " + string(conf.Fixed))
			}
		}
	}
}
func sendActivity(data map[string]map[string]map[string]interface{}, fts_server string, forwarder *Forwarder) {
	var err error
	var activity [][]map[string]interface{}
	var message []byte
	if activity = trans.TransformActivity(data, fts_server); len(activity) != 0 {
		for i := 0; i < len(activity); i++ {
			for j := 0; j < len(activity[i]); j++ {
				if message, err = json.Marshal(activity[i][j]); err != nil {
					log.WithError(err).Error("Creating message in " + string(conf.Activity))
				}
				if err = forwarder.ForwardSend(string(message), conf.Activity); err != nil {
					log.WithError(err).Error("Sending " + string(conf.Activity))
				}
			}
		}
	}
}

func sendSe(data map[string]map[string]map[string]interface{}, fts_server string, forwarder *Forwarder) {
	var err error
	var se []map[string]interface{}
	var message []byte
	if se = trans.TransformSe(data, fts_server); len(se) != 0 {
		for i := 0; i < len(se); i++ {
			if message, err = json.Marshal(se[i]); err != nil {
				log.WithError(err).Error("Creating message in " + string(conf.Se))
			}
			if err = forwarder.ForwardSend(string(message), conf.Se); err != nil {
				log.WithError(err).Error("Sending " + string(conf.Se))
			}
		}
	}
}

func sendGlobal(data map[string]map[string]interface{}, fts_server string, forwarder *Forwarder) {
	var err error
	var global []map[string]interface{}
	var message []byte
	if global = trans.TransformGlobal(data, fts_server); len(global) != 0 {
		for i := 0; i < len(global); i++ {
			if message, err = json.Marshal(global[i]); err != nil {
				log.WithError(err).Error("Creating message in " + string(conf.Global))
			}
			if err = forwarder.ForwardSend(string(message), conf.Global); err != nil {
				log.WithError(err).Error("Sending " + string(conf.Global))
			}
		}
	}
}

func CollectConfig(fts_server string, config_type conf.ConfigType, login string, passcode string) error {
	var err error
	var forwarder *Forwarder
	var url string
	log.Debug("start collecting")
	url = "https://" + fts_server + ":" + conf.FTS3ServerPort + "/config/" + string(config_type)
	cmd := exec.Command("curl", "-E", conf.Proxy, "--cacert", conf.Proxy, "--capath", conf.CaPath, url, "-H", "Accept: application/json")
	out, err := cmd.Output()
	log.Info("Output ", cmd)
	if err != nil {
		log.WithError(err).Error("Executing curl command")
		return err
	}

	if forwarder, err = NewForwarder(login, passcode); err != nil {
		return err
	}
	log.Debug("Created Forwarder")
	switch config_type {
	case conf.Shares:
		var data []map[string]interface{}
		json.Unmarshal(out, &data)
		sendShares(data, fts_server, forwarder)
	case conf.Fixed:
		var data []map[string]interface{}
		json.Unmarshal(out, &data)
		sendFixed(data, fts_server, forwarder)
	case conf.Links:
		var data []map[string]interface{}
		json.Unmarshal(out, &data)
		sendLinks(data, fts_server, forwarder)
	case conf.Global:
		var data map[string]map[string]interface{}
		json.Unmarshal(out, &data)
		sendGlobal(data, fts_server, forwarder)
	case conf.Se:
		var data map[string]map[string]map[string]interface{}
		json.Unmarshal(out, &data)
		sendSe(data, fts_server, forwarder)
	case conf.Activity:
		var data map[string]map[string]map[string]interface{}
		json.Unmarshal(out, &data)
		sendActivity(data, fts_server, forwarder)
	}
	return err
}
