/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package worker

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/stomp"
	conf "gitlab.cern.ch/fts/fts-config-collector/config"
)

type (
	// Forwarder subsystem sends local messages to the queue
	Forwarder struct {
		params     *stomp.ConnectionParameters
		sendParams stomp.SendParams
		producer   *stomp.Producer
	}
)

// New creates a new forwarder
func NewForwarder(login string, passcode string) (*Forwarder, error) {
	var err error
	forwarder := &Forwarder{}
	forwarder.params = conf.SetStompParams(login, passcode)
	forwarder.sendParams = stomp.SendParams{Persistent: true, ContentType: "application/json"}
	if forwarder.producer, err = stomp.NewProducer(*forwarder.params); err != nil {
		return nil, err
	}
	return forwarder, err
}

// forwardStart consumes local start messages and forward them to amqp.
func (forwarder *Forwarder) ForwardSend(message string, conf_type conf.ConfigType) error {

	if err := forwarder.producer.Send(
		conf.AMQTopic,
		string(message),
		forwarder.sendParams,
	); err != nil {
		return err
	}
	log.Info("Forwarded "+conf_type+" message", string(message))
	return nil
}
