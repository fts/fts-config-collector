/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package config

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.cern.ch/flutter/stomp"
)


// Control constants
const (
	AMQTopic       = "/topic/transfer.fts_monitoring_config"
	CaPath         = "/etc/grid-security/certificates"
	LogFile        = "fts-config-collector.log"
	FTS3ServerPort = "8446"
)

// Monit constants
const (
	Endpoint      = "endpnt"
	Source        = "src_se"
	Destination   = "dst_se"
	VO            = "vo"
	Activity_name = "activity"
	Weight        = "weight"
)

// Config constants
type ConfigType string

const (
	Global   ConfigType = "global"
	Se       ConfigType = "se"
	Fixed    ConfigType = "fixed"
	Links    ConfigType = "links"
	Shares   ConfigType = "shares"
	Activity ConfigType = "activity_shares"
)

var FTS3Servers = [...]string{"fts3.cern.ch", "fts3-atlas.cern.ch", "fts3-pilot.cern.ch",
	"fts3-test.gridpp.rl.ac.uk", "lcgfts3.gridpp.rl.ac.uk", "fts.usatlas.bnl.gov", "cmsfts3.fnal.gov"}
var ConfigTypes = [...]ConfigType{Global, Fixed, Se, Links, Shares, Activity}

var UserCert = "/afs/cern.ch/user/f/ftssuite/.globus/usercert.pem"
var UserKey  = "/afs/cern.ch/user/f/ftssuite/.globus/userkey.pem"
var Proxy    = "/afs/cern.ch/user/f/ftssuite/.globus/x509up_u86582"


func SetStompParams(login string, passcode string) *stomp.ConnectionParameters {
	params := &stomp.ConnectionParameters{}
	params.ClientID = uuid.NewV4().String()
	params.CaCert = ""
	params.CaPath = CaPath
	params.UserCert = UserKey
	params.UserKey = UserKey
	params.Insecure = false
	params.EnableTLS = false
	params.Login = login
	params.Passcode = passcode
	params.Address = "dashb-mb.cern.ch:61113"
	return params
}
