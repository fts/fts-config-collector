## Description

This project collects the configurations of all FTS Servers
(global, links, shares, se, fixed) and sends them to 
an activeMQ to be consumed by the monitoring infrastructure. 

## Setup

Currently, this project is run from Jenkins everyday from the job fts-config-collector.
(https://jenkins.cern.ch/fts-dmc/view/FTS-Collect/job/fts-config-collector/)

The configuration can be changed in the config package. 


