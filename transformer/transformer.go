/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package transformer

import (

	conf "gitlab.cern.ch/fts/fts-config-collector/config"
)

func getSEAttr(se string, attr map[string]map[string]interface{}, fts_server string, endpoint_type string) map[string]interface{} {
	for pos, value2 := range attr {
		if pos == endpoint_type {
			m := make(map[string]interface{})
			m["config_type"] = conf.Se
			m[conf.Endpoint] = fts_server
			if endpoint_type == "as_source" {
				m[conf.Source] = se
			} else {
				m[conf.Destination] = se
			}
			for info, value3 := range value2 {
				if info == "active" {
					if value3 != nil {
						m["max_active"] = value3
					}
					delete(m, "active")
				} else {
					m[info] = value3
				}
				if m["throughput"] == nil {
					delete(m, "throughput")
				}
			}
			return m
		}
	}
	return nil
}
func getSEOp(se string, attr map[string]map[string]interface{}, fts_server string, endpoint_type string) map[string]interface{} {
	for op, val0 := range attr {
		if op == "operations" {
			m := make(map[string]interface{})
			m["config_type"] = conf.Se
			m[conf.Endpoint] = fts_server
			m[conf.Source] = se
			m[conf.Destination] = se
			for vo, val1 := range val0 {
				m[conf.VO] = vo
				m["staging_limit"] = val1
			}
			return m
		}
	}
	return nil
}

func getActivities(vo string, attr map[string]map[string]interface{}, fts_server string) []map[string]interface{} {
	var activities []map[string]interface{}
	act := make(map[string]interface{})
	for active, value2 := range attr {
		if active != "share" {
			act["active"] = value2
		}
	}
	for pos, value2 := range attr {
		if pos == "share" {
			for activity, value3 := range value2 {
				m := make(map[string]interface{})
				m["config_type"] = conf.Activity
				m[conf.Endpoint] = fts_server
				m[conf.VO] = vo
				m["active"] = act["active"]
				m[conf.Activity_name] = activity
				m[conf.Weight] = value3
				activities = append(activities, m)
			}
		}

	}
	return activities
}

func TransformGlobal(data map[string]map[string]interface{}, fts_server string) []map[string]interface{} {
	var docs []map[string]interface{}
	for vo, val0 := range data {
		m := make(map[string]interface{})
		m[conf.VO] = vo
		m["config_type"] = conf.Global
		m[conf.Endpoint] = fts_server
		m["retry"] = 0
		m["max_per_link"] = 0
		m["optimizer_mode"] = 1
		m["show_user_dn"] = false
		m["global_timeout"] = 0
		m["max_time_queue"] = 0
		m["max_per_se"] = 0
		m["sec_per_mb"] = 0
		for attr, val1 := range val0 {
			m[attr] = val1
		}
		docs = append(docs, m)
	}
	return docs
}

func TransformSe(data map[string]map[string]map[string]interface{}, fts_server string) []map[string]interface{} {
	var docs []map[string]interface{}
	for se, value := range data {
		var source_doc map[string]interface{}
		source_doc = getSEAttr(se, value, fts_server, "as_source")
		if source_doc != nil {
			docs = append(docs, source_doc)
		}
		var dest_doc map[string]interface{}
		dest_doc = getSEAttr(se, value, fts_server, "as_destination")
		if dest_doc != nil {
			docs = append(docs, dest_doc)
		}
		var op_doc map[string]interface{}
		op_doc = getSEOp(se, value, fts_server, "as_destination")
		if op_doc != nil {
			docs = append(docs, op_doc)
		}
	}
	return docs
}

func TransformFixed(data []map[string]interface{}, fts_server string) []map[string]interface{} {
	for i := 0; i < len(data); i++ {
		data[i]["config_type"] = conf.Fixed
		data[i][conf.Endpoint] = fts_server
		source := data[i]["source_se"]
		delete(data[i], "source_se")
		data[i][conf.Source] = source
		destination := data[i]["dest_se"]
		delete(data[i], "dest_se")
		data[i][conf.Destination] = destination
		delete(data[i], "ema")
		delete(data[i], "active")
		delete(data[i], "datetime")
	}
	return data
}
func TransformShares(data []map[string]interface{}, fts_server string) []map[string]interface{} {
	for i := 0; i < len(data); i++ {
		data[i]["config_type"] = conf.Shares
		data[i][conf.Endpoint] = fts_server
		source := data[i]["source"]
		delete(data[i], "source")
		data[i][conf.Source] = source
		destination := data[i]["destination"]
		delete(data[i], "destination")
		data[i][conf.Destination] = destination
	}
	return data
}
func TransformLinks(data []map[string]interface{}, fts_server string) []map[string]interface{} {
	for i := 0; i < len(data); i++ {
		data[i]["config_type"] = conf.Links
		data[i][conf.Endpoint] = fts_server
		source := data[i]["source"]
		delete(data[i], "source")
		data[i][conf.Source] = source
		destination := data[i]["destination"]
		delete(data[i], "destination")
		data[i][conf.Destination] = destination
	}
	return data
}

func TransformActivity(data map[string]map[string]map[string]interface{}, fts_server string) [][]map[string]interface{} {
	var docs [][]map[string]interface{}
	for vo, value := range data {
		var activities []map[string]interface{}
		activities = getActivities(vo, value, fts_server)
		if activities != nil {
			docs = append(docs, activities)
		}
	}
	return docs
}
