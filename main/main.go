/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"flag"
    "time"
	log "github.com/Sirupsen/logrus"
	conf "gitlab.cern.ch/fts/fts-config-collector/config"
	"gitlab.cern.ch/fts/fts-config-collector/worker"
)

func main() {
	var err error
	var fts_server string
	var config_type conf.ConfigType
	var login = flag.String("login", "ftspublisher", "login for stomp message queue")
	var passcode = flag.String("passwd", "", "password for stomp message queue")
	var user_cert = flag.String("cert", "", "user certificate")
	var user_key = flag.String("key", "", "user key")
	var proxy = flag.String("proxy", "", "proxy path")
	flag.Parse()
	//util.RedirectLog(conf.LogFile)
	if len(*user_cert) > 0 && len(*user_key) > 0 && len(*proxy) > 0 {
		conf.UserCert = *user_cert
		conf.UserKey = *user_key
		conf.Proxy = *proxy
		log.Info("Using parameters")
	} else {
		log.Info("Using default credentials")
	}
	if len(*login) == 0 || len(*passcode) == 0 {
		log.Error("No stomp login or passcode provided")
	}
	for i := 0; i < len(conf.FTS3Servers); i++ {
		fts_server = conf.FTS3Servers[i]
		for j := 0; j < len(conf.ConfigTypes); j++ {
			config_type = conf.ConfigTypes[j]
			if err = worker.CollectConfig(fts_server, config_type, *login, *passcode); err != nil {
				log.WithError(err).Error("Collecting configuration " + string(config_type) + " in the FTS Server " + fts_server)
				time.Sleep(100 * time.Millisecond)
			}
		}
	}
}
